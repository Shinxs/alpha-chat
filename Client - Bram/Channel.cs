﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientBram
{
    public class Channel
    {
        public String name { get; set; }
        public String uid;
        public String html;

        public Channel(String name,String uid,String html= "<!DOCTYPE html><body onload = 'window.scrollTo(0,document.body.scrollHeight);'>")
        {
            this.name = name;
            this.uid = uid;
            this.html = html;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace ClientBram
{
    public partial class ClientWindow : Window
    {
        public String ip = "localhost";
        public int port = 5623;
        public String nick = "user";
        public Boolean listening = false;
        public List<Channel> channels = new List<Channel>();
        public Channel currentChannel;
        private TcpClient client;
        private NetworkStream stream;
        public Thread t;
        private ClientCommands commands;

        public String messageStyling = "margin: 0; background-color: #EEEEEE; padding: 2px; border-bottom: #AAA solid 1px; font-size: 14px;";
        public String messageStylingName = "margin: 0; background-color: #EEC2C2; padding: 2px; border-bottom: #AAA solid 1px; font-size: 14px;";

        public int messagesIndex = 0;
        public List<String> messages = new List<String>();

        /*
         * Initializer
         */
        public ClientWindow()
        {
            InitializeComponent();
            addChannel("*none*", Guid.NewGuid().ToString());

            btnSend.Click += new RoutedEventHandler(sendOnClick);
            tbMessage.GotFocus += new RoutedEventHandler(tbRemoveText);
            tbMessage.LostFocus += new RoutedEventHandler(tbAddText);
            tbMessage.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(keyPressed);
            wbMessages.DocumentText = channels[0].name;
            lvChannels.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(SelectChannel);

            addTextChannelName("Alph&alpha; Ch&alpha;t Client | Version 0.1<br/>");

            while((nick = Interaction.InputBox("Nickname:","Client","")).Length == 0);

            tbMessage.Focus();

            t = new Thread(new ThreadStart(startClient));
            commands = new ClientCommands(this);
        }

        public void startClient()
        {
            try
            {
                client = new TcpClient(ip, port);
                while(!client.Connected);
                stream = client.GetStream();
                listening = true;
                sendPacket(new Packet("Server", nick, Types.CONNECT, nick));
            }
            catch
            {
                addTextChannelName("Connection Timeout");
            }

            while(listening)
            {
                String p = recievePacket();
                if(p.Length > 0)
                    processPacket(p);
            }
            addTextChannelName("Disconnecting....");
            if(client != null && client.Connected)
                client.Close();
            t.Abort();
            t.Join();
        }

        /*
         * Waiting for a packet
         */
        public String recievePacket()
        {
            try
            {
                byte[] bytes = new byte[4096];
                stream.Read(bytes,0,bytes.Length);
                String p = System.Text.Encoding.UTF8.GetString(bytes);
                p = p.Split(new char[] { '\0' },2)[0];
                List<String> b64 = new List<String>();
                int i = 0;
                int ie = 0;
                foreach(char c in p)
                {
                    if(c.Equals('='))
                    {
                        if(p.Length > ie+1 && !p[ie+1].Equals('='))
                        {
                            ie = 0;
                            i++;
                        }
                        else
                            b64[i] += "=";
                    }
                    else
                    {
                        if(b64.Count == i)
                            b64.Add(c.ToString());
                        else
                            b64[i] += c;
                    }
                    ie++;
                }
                for(i = 0;i < b64.Count();i++)
                {
                    debug(i);
                    b64[i] = decrypt(b64[i]);
                }
                String json = "["+String.Join(",",b64)+"]";
                while(json.Contains("}{"))
                    json.Replace("}{", "},{");
                return json;
            }
            catch
            {
                addTextChannelName("Error");
            }
            return "";
        }

        /*
         * Processing a received packet
         */
        public void processPacket(String packet)
        {
            //addText(packet);
            List<Packet> packets = new JavaScriptSerializer().Deserialize<List<Packet>>(packet);
            foreach(Packet p in packets)
            {
                if(p.type == Types.CONNECT)
                {
                    if(p.data.Split(' ')[0].Equals("success"))
                    {
                        addChannel(p.data.Split(' ')[1],p.data.Split(' ')[2]);
                    }
                }
                if(p.type == Types.NICK)
                {
                    if(p.source.ToLower().Equals("server"))
                    {
                        if(p.data.Split(' ')[0].Equals("success"))
                            nick = p.data.Split(' ')[1];
                        else if(p.data.Split(' ')[0].Equals("invalid"))
                        {
                            addText(new Packet(nick, "Server", Types.MESSAGE, "That nickname is invalid"));
                            //nick = Interaction.InputBox("Nickname:","Nickname","");
                        }
                    }
                    else
                    {
                        String oldnick = p.source;
                        String newnick = p.data;
                        addText(new Packet("Server","Server",Types.MESSAGE,oldnick + " changed his nick to " + newnick));
                    }
                }
                else if(p.type == Types.MESSAGE)
                {
                    addText(p);
                }
            }
        }

        /*
         * Process any message that get entered in the server console
         */
        public void processMessage(String msg)
        {
            String cmd = msg.Split(' ')[0].ToLower();
            if(commands.commands.ContainsKey(cmd))
                commands.commands[cmd](msg.Split(' ').Skip(1).ToArray());
            else
            {
                sendPacket(new Packet("#Main",nick,Types.MESSAGE,msg));
                addText(new Packet("#Main",nick,Types.MESSAGE,msg));
            }
        }

        /*
         * Send a packet to this specific client
         */
        public void sendPacket(Packet p)
        {
            try
            {
                var json = new JavaScriptSerializer().Serialize(p);
                byte[] bytes = Encoding.UTF8.GetBytes(encrypt(json));
                stream.Write(bytes,0,bytes.Length);
                stream.Flush();
            }
            catch
            {
                addTextChannelName("Error");
            }
        }

        /*
         * Encrypt the packet
         */
        public String encrypt(String packet)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(packet);
            return System.Convert.ToBase64String(bytes);
        }

        /*
         * Descrypt the packet
         */
        public String decrypt(String packet)
        {
            byte[] bytes = System.Convert.FromBase64String(packet);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        /*
         * Adding text to the messages (from packet)
         */
        public void addText(Packet p)
        {
            if(p.type == Types.MESSAGE)
            {
                addTextChannelGuid(p.destination,p.source + ": " + p.data);
            }
        }

        /*
         * Adding text to the messages (from string)
         */
        public void addTextChannelName(String msg,String channelName = "*none*")
        {
            Channel c = getChannelByName(channelName);
            if(c != null)
            {
                c.html += msg;
                setHtml(c.html);
            }
        }

        public void addTextChannelGuid(String msg,String channelGuid)
        {
            Channel c = getChannelByGuid(channelGuid);
            c.html += msg;
            setHtml(c.html);
        }

        /*
         * Set html (from string)
         */
        public void setHtml(String html)
        {
            Dispatcher.BeginInvoke((MethodInvoker)delegate {
                wbMessages.DocumentText = html;
            });
        }

        /*
         * Adding a channel
         */
        public void addChannel(String name, String uid)
        {
            Channel c = new Channel(name,uid);
            channels.Add(c);
            currentChannel = c;
            Dispatcher.BeginInvoke((MethodInvoker)delegate {
                lvChannels.Items.Add(c);
            });
        }

        /*
         * Get channel by name
         */
        public Channel getChannelByName(String name)
        {
            foreach(Channel c in channels)
            {
                if(c.name.Equals(name))
                    return c;
            }
            return null;
        }

        /*
         * Get channel by guid
         */
        public Channel getChannelByGuid(String guid)
        {
            foreach(Channel c in channels)
            {
                if(c.uid.Equals(guid))
                    return c;
            }
            return null;
        }

        /* 
         * Event Handlers
         */
        public void sendOnClick(object sender,RoutedEventArgs e)
        {
            processMessage(tbMessage.Text);
            tbMessage.Text = "";
        }

        public void keyPressed(object sender,System.Windows.Input.KeyEventArgs e)
        {
            if(e.IsDown && e.Key == Key.Enter && tbMessage.Text.Length > 0)
            {
                messages.Add(tbMessage.Text);
                processMessage(tbMessage.Text);
                tbMessage.Text = "";
            }
            if(e.IsDown && e.Key == Key.Up)
            {
                if(messagesIndex > 0)
                {
                    messagesIndex--;
                    tbMessage.Text = messages[messagesIndex];
                }
            }
            if(e.IsDown && e.Key == Key.Down)
            {
                if(messagesIndex < messages.Count()-1)
                {
                    messagesIndex++;
                    tbMessage.Text = messages[messagesIndex];
                }
                else
                {
                    tbMessage.Text = "";
                }
            }
        }

        public void tbRemoveText(object sender,RoutedEventArgs e)
        {
            tbMessage.Text = "";
        }

        public void tbAddText(object sender,RoutedEventArgs e)
        {
            if(String.IsNullOrWhiteSpace(tbMessage.Text))
                tbMessage.Text = "Message";
        }

        public void SelectChannel(object sender,MouseButtonEventArgs e)
        {
            var item = (sender as System.Windows.Controls.ListView).SelectedItem;
            if(item != null)
            {
                int index = lvChannels.SelectedIndex;
                setHtml(channels[index].html);
            }
        }

        public void debug(object msg)
        {
            System.Windows.Forms.MessageBox.Show("" + msg);
        }
    }
}

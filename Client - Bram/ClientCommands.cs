﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientBram
{
    class ClientCommands
    {
        private ClientWindow client;
        public Dictionary<String,Action<String[]>> commands = new Dictionary<String,Action<String[]>>();

        public ClientCommands(ClientWindow client)
        {
            this.client = client;
            this.commands.Add("/help",help);
            this.commands.Add("/connect",connect);
            this.commands.Add("/nick",nick);
        }

        /*
        * Client command for help
        */
        public void help(String[] args)
        {
            String txt;
            txt = "--- Command Help ---<br/>";
            for(int i = 1; i < commands.Count(); i++)
                txt += commands.Keys.ElementAt(i) + " help<br/>";
            txt += "-------------------<br/>";
            client.addTextChannelName(txt);
        }

        /*
        * Client command for connecting to server
        */
        public void connect(String[] args)
        {
            int p;
            String txt;
            txt = "--- Connect Help ---<br/>";
            txt += "/connect &lt;ip&gt;           | Connnect to ip on port " + client.port + "<br/>";
            txt += "/connect &lt;ip&gt; &lt;port&gt;    | Display this message<br/>";
            txt += "/connect help           | Display this message<br/>";
            txt += "-------------------<br/>";
            if(args.Length > 0 && !args[0].ToLower().Equals("help"))
            {
                client.ip = args[0];
                client.t.Start();
            }
            else if(args.Length > 1 && args[0].ToLower().Equals("help") && Int32.TryParse(args[1],out p))
            {
                client.ip = args[0];
                client.port = p;
                client.t.Start();
            }
            else
            {
                client.addTextChannelName(txt);
            }
        }

        /*
        * Client command for changing nick
        */
        public void nick(String[] args)
        {
            String txt;
            txt = "--- Nick Help ---<br/>";
            txt += "/nick &lt;new nick&gt;       | Change your nickname<br/>";
            txt += "/nick help           | Display this message<br/>";
            txt += "-------------------<br/>";
            if(args.Length > 0 && !args[0].ToLower().Equals("help"))
            {
                client.sendPacket(new Packet("Server",client.nick,Types.NICK,args[0]));
            }
            else if(args.Length > 0 && args[0].ToLower().Equals("help"))
            {
                client.addTextChannelName(txt);
            }
            else
            {
                client.addTextChannelName(txt);
            }
        }
    }
}

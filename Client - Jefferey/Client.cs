﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Client___Jefferey
{
    public class Client
    {

        public Connection conn; // Prepare connection object
        public string nick;

        public static string Serialize(object obj)
        {
            return Convert.ToString(new JavaScriptSerializer().Serialize(obj));
        }

        public static string PrepDataSend(object obj)
        {
            string sendableData;
            string json;
            json = Serialize(obj);
            sendableData = Crypt.Encrypt(json);
            return sendableData;
        }

        // Convert received data to Packet object
        public static Packet toPacket(string obfuscatedData)
        {
            return new JavaScriptSerializer().Deserialize<Packet>(Crypt.Decrypt(obfuscatedData)); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace Client___Jefferey
{
   public class Connection
    {
        public string message ;
        public string serverIp ; // Default server address
        public Int32 serverPort ; // Default server port
        public TcpClient connection ;
        public StreamReader getData ;
        public StreamWriter sendData ;

        // Call to set connection
        public Connection(int SERVERPORT=5623, string SERVERIP="localhost")
        {
            serverPort = SERVERPORT;
            serverIp = SERVERIP;
            connection = new TcpClient(serverIp, serverPort);
            getData = new StreamReader(connection.GetStream());
            sendData = new StreamWriter(connection.GetStream());
        }

        // Default connection options
        public Connection()
        {
            serverPort = 5623;
            serverIp = "localhost";
            connection = new TcpClient(serverIp, serverPort);
            getData = new StreamReader(connection.GetStream());
            sendData = new StreamWriter(connection.GetStream());
        }


        // Call to send data to server
        public void SendData(string data)
        {
            try
            {
                sendData.WriteLine(data);
                sendData.Flush();
            }
            catch (Exception e)
            {
                Console.WriteLine(e); 
            }
            
        }

        public string GetData()
        {
            return getData.ReadLine();
        }

    }

}


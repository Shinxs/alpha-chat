﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client___Jefferey
{
    public class Crypt
    {
        // Consctructor
        public Crypt()
        {
        }

        // Encrypt data to send
        public static string Encrypt(string plainTextMessage)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainTextMessage);
            return System.Convert.ToBase64String(plainTextBytes); // Convert data to Base64 (Placeholder code for real encryption)
        }

        // Decrypt data to read
        public static string Decrypt(string encodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(encodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes); // Convert Base64 to text (Placeholder code for real decryption)
        }
    }
}

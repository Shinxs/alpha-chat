﻿namespace Client___Jefferey
{
    partial class Gui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.TextBox();
            this.lbChat = new System.Windows.Forms.ListBox();
            this.tbNick = new System.Windows.Forms.TextBox();
            this.lblNick = new System.Windows.Forms.Label();
            this.tbServer = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSetNick = new System.Windows.Forms.Button();
            this.tbTarget = new System.Windows.Forms.TextBox();
            this.lblTarget = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(641, 283);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(42, 20);
            this.btnSend.TabIndex = 13;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(11, 283);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(624, 20);
            this.tbMessage.TabIndex = 12;
            this.tbMessage.TextChanged += new System.EventHandler(this.tbMessage_TextChanged);
            // 
            // lbChat
            // 
            this.lbChat.FormattingEnabled = true;
            this.lbChat.Location = new System.Drawing.Point(11, 32);
            this.lbChat.Name = "lbChat";
            this.lbChat.Size = new System.Drawing.Size(672, 238);
            this.lbChat.TabIndex = 11;
            this.lbChat.SelectedIndexChanged += new System.EventHandler(this.lbChat_SelectedIndexChanged);
            // 
            // tbNick
            // 
            this.tbNick.Location = new System.Drawing.Point(535, 4);
            this.tbNick.Name = "tbNick";
            this.tbNick.Size = new System.Drawing.Size(100, 20);
            this.tbNick.TabIndex = 10;
            this.tbNick.Text = "Jefferey";
            // 
            // lblNick
            // 
            this.lblNick.AutoSize = true;
            this.lblNick.Location = new System.Drawing.Point(497, 7);
            this.lblNick.Name = "lblNick";
            this.lblNick.Size = new System.Drawing.Size(32, 13);
            this.lblNick.TabIndex = 9;
            this.lblNick.Text = "Nick:";
            this.lblNick.Click += new System.EventHandler(this.lblNick_Click);
            // 
            // tbServer
            // 
            this.tbServer.Location = new System.Drawing.Point(59, 6);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(100, 20);
            this.tbServer.TabIndex = 8;
            this.tbServer.Text = "localhost";
            this.tbServer.TextChanged += new System.EventHandler(this.tbServer_TextChanged);
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(12, 9);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(41, 13);
            this.lblServer.TabIndex = 7;
            this.lblServer.Text = "Server:";
            this.lblServer.Click += new System.EventHandler(this.lblServer_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(165, 6);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(59, 20);
            this.btnConnect.TabIndex = 14;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSetNick
            // 
            this.btnSetNick.Location = new System.Drawing.Point(641, 4);
            this.btnSetNick.Name = "btnSetNick";
            this.btnSetNick.Size = new System.Drawing.Size(41, 20);
            this.btnSetNick.TabIndex = 15;
            this.btnSetNick.Text = "Set";
            this.btnSetNick.UseVisualStyleBackColor = true;
            this.btnSetNick.Click += new System.EventHandler(this.btnSetNick_Click);
            // 
            // tbTarget
            // 
            this.tbTarget.Location = new System.Drawing.Point(277, 6);
            this.tbTarget.Name = "tbTarget";
            this.tbTarget.Size = new System.Drawing.Size(100, 20);
            this.tbTarget.TabIndex = 17;
            this.tbTarget.TextChanged += new System.EventHandler(this.tbTarget_TextChanged);
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Location = new System.Drawing.Point(230, 9);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(41, 13);
            this.lblTarget.TabIndex = 16;
            this.lblTarget.Text = "Target:";
            this.lblTarget.Click += new System.EventHandler(this.lblTarget_Click);
            // 
            // Gui
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 313);
            this.Controls.Add(this.tbTarget);
            this.Controls.Add(this.lblTarget);
            this.Controls.Add(this.btnSetNick);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.lbChat);
            this.Controls.Add(this.tbNick);
            this.Controls.Add(this.lblNick);
            this.Controls.Add(this.tbServer);
            this.Controls.Add(this.lblServer);
            this.Name = "Gui";
            this.Text = "Alphα Chαt - Dwarvy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox tbMessage;
        private System.Windows.Forms.ListBox lbChat;
        private System.Windows.Forms.TextBox tbNick;
        private System.Windows.Forms.Label lblNick;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSetNick;
        private System.Windows.Forms.TextBox tbTarget;
        private System.Windows.Forms.Label lblTarget;
    }
}


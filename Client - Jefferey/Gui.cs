﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Threading;


namespace Client___Jefferey
{
    public partial class Gui : Form
    {

        public Client client = new Client();
        public Thread listen;

        public Gui()
        {
            InitializeComponent();

        }


        // Connect button
        public void btnConnect_Click(object sender, EventArgs e)
        {
            client.conn = new Connection(5623, tbServer.Text); // Connect to default port on given server
            client.conn.SendData(Client.PrepDataSend(new Packet("Server",tbNick.Text,Types.CONNECT,""))); // Send connection message to server
            listen = new Thread(listenThread);
            listen.Start();
        }


        public void addToChat(string text) // Add text to chat window
        {
            lbChat.Items.Add(text);
            lbChat.SelectedIndex = lbChat.Items.Count - 1;
        }


        public void listenThread() // Listen for incomming data from server
        {
            while (true)
            {
                try
                {
                    Packet p = Client.toPacket(client.conn.GetData());
                    if (p.data == "" && new List<Types>{Types.NICK, Types.MESSAGE, Types.IMAGE, Types.FILE }.Contains(p.type)) // if data is missing for type that *NEEDS* data
                    {
                        p = null; // Discard the packet
                    }
                    while (p != null)
                    {
                        if (InvokeRequired)
                        {
                            string text = "";
                            switch (p.type)
                            {
                                case Types.NONE:
                                    break;
                                case Types.NICK:
                                    text = p.source + " is no known as " + p.data;
                                    break;
                                case Types.CONNECT:
                                    text = p.source + " has joined the chat";
                                    break;
                                case Types.DISCONNECT:
                                    text = p.source + " has left the chat";
                                    break;
                                case Types.RENAMED:
                                    break;
                                case Types.MESSAGE:
                                    text = p.source + ": " + p.data;
                                    break;
                                case Types.IMAGE:
                                    text = p.source + " sent you an image but this not supported yet.";
                                    break;
                                case Types.FILE:
                                    text = p.source + " sent you a file but this is not supported yet";
                                    break;
                                default:
                                    text = "Someone sent you some data your chat-client doesn't understand";
                                    break;
                            }
                            p = null; // Discard the packet
                            this.Invoke(new Action(() => addToChat(text))); // Add received data to chat window 
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong in function listenThread(): \r\n " + e);
                }
            }
        }


        // Send button
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (tbMessage.Text != null)
            {
                lbChat.Items.Add("Me: "+tbMessage.Text);
                lbChat.SelectedIndex = lbChat.Items.Count - 1;
                client.conn.SendData(Client.PrepDataSend(new Packet(tbTarget.Text, tbNick.Text, Types.MESSAGE, tbMessage.Text)));
                tbMessage.Clear();
            }
        }


        private void btnSetNick_Click(object sender, EventArgs e) 
        {
            client.conn.SendData(Client.PrepDataSend(new Packet(tbTarget.Text,client.nick,Types.NICK,tbNick.Text)));
            client.nick = tbNick.Text;
        }

        private void tbMessage_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbChat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblNick_Click(object sender, EventArgs e)
        {

        }

        private void tbServer_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblServer_Click(object sender, EventArgs e)
        {

        }

        private void tbTarget_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblTarget_Click(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client___Jefferey
{
    public class Packet
    {
        public String destination = "";
        public String source = "";
        public Types type = Types.NONE;
        public String data = "";

        public Packet()
        {
        }

        public Packet(String DESTINATION, String SOURCE, Types TYPE, String DATA)
        {
            destination = DESTINATION;
            source = SOURCE;
            type = TYPE;
            data = DATA;
        }
    }

    public enum Types
    {
        NONE = 0,
        NICK = 1,
        CONNECT = 2,
        DISCONNECT = 3,
        RENAMED = 4,
        MESSAGE = 5,
        IMAGE = 6,
        FILE = 7,
    }
}

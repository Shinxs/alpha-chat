﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.VisualBasic;
using System.Web.Script.Serialization;

namespace ClientJefferey
{
    public partial class Client : Form
    {
        // Init
        public Client()
        {
            InitializeComponent();
        }

        // Create essential objects
        Crypt crypt = new Crypt();
        Connection connection = new Connection();

        // Make json from object (packet)
        public string MakeJson(object packet)
        {
            return new JavaScriptSerializer().Serialize(packet);
        }

        private void Client_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}

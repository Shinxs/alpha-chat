﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.VisualBasic;
using System.Web.Script.Serialization;

namespace ClientJefferey
{
    public partial class ClientWindow : Window
    {
        Crypt crypt = new Crypt();
        Connection connection = new Connection();

        // Make json van object
        public string Serialize(object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }

    }
}



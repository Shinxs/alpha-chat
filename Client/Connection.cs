﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace ClientJefferey
{
   public class Connection
    {
        public string message ;
        public string serverIp ; // Default server address
        public Int32 serverPort ; // Default server port
        public TcpClient connection ;
        public StreamReader getData ;
        public StreamWriter sendData ;

        // Call to set connection
        public Connection(int SERVERPORT, string SERVERIP)
        {
            this.serverPort = SERVERPORT;
            this.serverIp = SERVERIP;
            this.connection = new TcpClient(serverIp, serverPort);
            this.getData = new StreamReader(connection.GetStream());
            this.sendData = new StreamWriter(connection.GetStream());
        }

        // Default connection options
        public Connection()
        {
            this.serverPort = 5623;
            this.serverIp = "localhost";
            this.connection = new TcpClient(serverIp, serverPort);
            this.getData = new StreamReader(connection.GetStream());
            this.sendData = new StreamWriter(connection.GetStream());
        }


        // Call to send data to server
        public void SendData(string data)
        {
            this.sendData.WriteLine(data);
            this.sendData.Flush();
        }

        public void GetData(string data)
        {
            this.sendData.WriteLine(data);
            this.sendData.Flush();
        }

    }

}


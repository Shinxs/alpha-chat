﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientJefferey
{
    class Crypt
    {
        // Encrypt data to send
        public string Encrypt(string plainTextMessage)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainTextMessage);
            return System.Convert.ToBase64String(plainTextBytes); // Convert data to Base64 (Placeholder code for real encryption)
        }

        // Decrypt data to read
        public string Decrypt(string encodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(encodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes); // Convert Base64 to text (Placeholder code for real decryption)
        }
    }
}

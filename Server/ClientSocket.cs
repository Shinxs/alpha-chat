﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Web.Script.Serialization;

namespace Server
{
    public class Client
    {
        private ServerWindow server;
        private TcpClient client;
        public String nick = "";
        private NetworkStream stream;
        public Thread t;

        public Client(TcpClient client, ServerWindow server)
        {
            this.client = client;
            this.stream = client.GetStream();
            this.server = server;

            t = new Thread(new ThreadStart(start));
            t.Start();
        }

        /*
         * This starts listening for packets
         * When packet is recieved it's processed 
         */
        public void start()
        {
            while(t.IsAlive)
            {
                String p = recievePacket();
                if(p.Length > 0) processPacket(p);
            }
        }

        /*
         * Waiting for a packet
         */
        public String recievePacket()
        {
            try
            {
                byte[] bytes = new byte[4096];
                int length = stream.Read(bytes,0,bytes.Length);
                String p = System.Text.Encoding.UTF8.GetString(bytes);
                p = p.Split(new char[] {'\0'},2)[0];
                return decrypt(p);
            }
            catch {
                if(client.Connected)
                    client.GetStream().Close();
                client.Close();
                server.broadcast(new Packet("Main", nick, Types.DISCONNECT, nick + " has disconnected"));
                server.removeClient(this);
            }
            return "";
        }

        /*
         * Processing a received packet
         */
        public void processPacket(String packet)
        {
            Packet p = new JavaScriptSerializer().Deserialize<Packet>(packet);
            if(p.type == Types.CONNECT)
            {
                if(!String.IsNullOrWhiteSpace(p.source) && !server.nickInUse(p.source))
                {
                    nick = p.source;
                    server.addText(new Packet(server.nick,server.nick,Types.MESSAGE,p.source + " has connected"));
                    sendPacket(new Packet(nick,server.nick,Types.CONNECT,"success " + server.servername + " " + server.guid.ToString()));
                    server.broadcast(new Packet(nick,server.nick,Types.NICK,p.source + " has connected"));
                    sendPacket(new Packet(nick,server.nick,Types.MESSAGE,"You are connected as " + nick));
                }
                else if(String.IsNullOrWhiteSpace(p.source))
                {
                    sendPacket(new Packet(p.source,server.nick,Types.NICK,"invalid"));
                }
                else
                {
                    sendPacket(new Packet(p.source,server.nick,Types.MESSAGE,"That nickname is already in use"));
                }
            }
            else if(p.type == Types.NICK)
            {
                if(!String.IsNullOrWhiteSpace(p.data) && !server.nickInUse(p.data))
                {
                    String oldnick = p.source;
                    String newnick = p.data;
                    nick = newnick;
                    sendPacket(new Packet(nick,server.nick,Types.NICK,"success " + p.data));
                    server.addText(new Packet(server.nick,server.nick,Types.MESSAGE,oldnick + " changed his nick to " + newnick));
                    server.broadcast(p);
                }
                else if(String.IsNullOrWhiteSpace(p.data))
                {
                    sendPacket(new Packet(p.source,server.nick,Types.NICK,"invalid"));
                }
                else
                {
                    sendPacket(new Packet(p.source,server.nick,Types.MESSAGE,"That nickname is already in use"));
                }
            }
            else if(p.type == Types.MESSAGE)
            {
                server.addText(p);
                server.broadcast(p);
            }
        }

        /*
         * Send a packet to this specific client
         */
        public void sendPacket(Packet p)
        {
            try
            {
                var json = new JavaScriptSerializer().Serialize(p);
                byte[] bytes = Encoding.UTF8.GetBytes(encrypt(json));
                stream.Write(bytes,0,bytes.Length);
                stream.Flush();
                Thread.Sleep(10);
            }
            catch {
                if(client.Connected)
                    client.GetStream().Close();
                client.Close();
                server.broadcast(new Packet("Main",nick,Types.DISCONNECT,nick + " has disconnected"));
                server.removeClient(this);
            }
        }

        /*
         * Change the nickname for this client
         */
        public void setNick(String nick)
        {
            this.nick = nick;
        }

        /*
         * Encrypt the packet
         */
        public String encrypt(String packet)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(packet);
            return System.Convert.ToBase64String(bytes);
        }

        /*
         * Descrypt the packet
         */
        public String decrypt(String packet)
        {
            byte[] bytes = System.Convert.FromBase64String(packet);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
    }
}
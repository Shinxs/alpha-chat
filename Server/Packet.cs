﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Packet
    {
        public String destination = "";
        public String source = "";
        public Types type = Types.NONE;
        public String data = "";

        public Packet()
        {
        }

        public Packet(String des, String source, Types type, String data)
        {
            this.destination = des;
            this.source = source;
            this.type = type;
            this.data = data;

        }
    }

    public enum Types
    {
        NONE = 0,
        NICK = 1,
        CONNECT = 2,
        DISCONNECT = 3,
        RENAMED = 4,
        MESSAGE = 5,
        IMAGE = 6,
        FILE = 7,
    }
}

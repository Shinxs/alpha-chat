﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Server
{
    public partial class ServerWindow : Window
    {
        public String ip = "127.0.0.1";
        public int port = 5623;
        public String nick = "Server";
        public Boolean listening = false;
        public String html = "<!DOCTYPE html><body onload='window.scrollTo(0,document.body.scrollHeight);'>";
        public Thread t;
        private ServerCommands commands;
        public String servername = "Alphα Chαt";
        public Guid guid = Guid.NewGuid();

        private List<Client> clients = new List<Client>();

        public String messageStyling = "margin: 0; background-color: #EEEEEE; padding: 2px; border-bottom: #AAA solid 1px; font-size: 14px;";

        public int messagesIndex = 0;
        public List<String> messages = new List<String>();

        /*
         * Initializer
         */
        public ServerWindow()
        {
            InitializeComponent();

            btnSend.Click += new RoutedEventHandler(sendOnClick);
            tbMessage.GotFocus += new RoutedEventHandler(tbRemoveText);
            tbMessage.LostFocus += new RoutedEventHandler(tbAddText);
            tbMessage.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(keyPressed);
            wbMessages.DocumentText = html;

            addText("Alph&alpha; Ch&alpha;t Server | Version 0.0.1<br/>");

            tbMessage.Focus();

            t = new Thread(new ThreadStart(startServer));
            commands = new ServerCommands(this);
            t.Start();
        }

        /*
         * Start the server
         */
        public void startServer()
        {
            TcpListener server = new TcpListener(IPAddress.Parse(ip),port);
            addText("Starting server....");
            server.Start();
            listening = true;
            addText("Server started on port " + port);

            while(listening)
            {
                Thread.Sleep(10);
                TcpClient client = server.AcceptTcpClient();
                clients.Add(new Client(client,this));
            }
            addText("Stopping server....");
            server.Stop();
            t.Abort();
            t.Join();
        }

        /*
         * Process any message that get entered in the server console
         */
        public void processMessage(String msg)
        {
            String cmd = msg.Split(' ')[0].ToLower();
            if(commands.commands.ContainsKey(cmd))
                commands.commands[cmd](msg.Split(' ').Skip(1).ToArray());
            else
            {
                broadcast(new Packet("Main",nick,Types.MESSAGE,msg));
                addText(new Packet("Main",nick,Types.MESSAGE,msg));
            }
        }

        /*
         * Relay a message to all connected clients
         */
        public void broadcast(Packet p)
        {
            foreach(Client c in clients)
            {
                if(!c.nick.ToLower().Equals(p.source.ToLower()))
                {
                    c.sendPacket(p);
                }
            }
        }

        /*
         * Adding text to the messages (from packet)
         */
        public void addText(Packet p)
        {
            if(p.type == Types.MESSAGE)
            {
                Dispatcher.BeginInvoke((MethodInvoker)delegate {
                    wbMessages.DocumentText += "<p style='" + messageStyling + "'>" + p.source + ": " + p.data + "</p>";
                });
            }
        }

        /*
         * Adding text to the messages (from string)
         */
        public void addText(String msg)
        {
            Dispatcher.BeginInvoke((MethodInvoker)delegate {
                wbMessages.DocumentText += "<p style='" + messageStyling + "'>" + msg + "</p>";
            });
        }

        /*
         * A function for checking whether a nickname is alreay in use
         */
        public bool nickInUse(String nick)
        {
            bool used = false;
            foreach(Client c in clients)
            {
                if(c.nick.ToLower().Equals(nick.ToLower()))
                {
                    used = true;
                    break;
                }
            }
            return used;
        }

        /*
         * Remove/Disconnect a client from server
         */
        public void removeClient(Client c)
        {
            c.t.Abort();
            c.t.Join();
            clients.Remove(c);
        }

        /* 
         * Event Handlers
         */
        public void sendOnClick(object sender, RoutedEventArgs e)
        {
            processMessage(tbMessage.Text);
            tbMessage.Text = "";
        }

        public void keyPressed(object sender,System.Windows.Input.KeyEventArgs e)
        {
            if(e.IsDown && e.Key == Key.Enter)
            {
                messages.Add(tbMessage.Text);
                processMessage(tbMessage.Text);
                tbMessage.Text = "";
                messagesIndex = messages.Count();
            }
            if(e.IsDown && e.Key == Key.Up)
            {
                if(messagesIndex > 0)
                {
                    messagesIndex--;
                    tbMessage.Text = messages[messagesIndex];
                }
            }
            if(e.IsDown && e.Key == Key.Down)
            {
                if(messagesIndex < messages.Count()-1)
                {
                    messagesIndex++;
                    tbMessage.Text = messages[messagesIndex];
                }
                else
                {
                    tbMessage.Text = "";
                }
            }
        }

        public void tbRemoveText(object sender, RoutedEventArgs e)
        {
            tbMessage.Text = "";
            messagesIndex = messages.Count();
        }

        public void tbAddText(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbMessage.Text))
                tbMessage.Text = "Message";
        }
    }
}

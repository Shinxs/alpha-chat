﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class ServerCommands
    {
        private ServerWindow server;
        public Dictionary<String,Action<String[]>> commands = new Dictionary<String,Action<String[]>>();

        public ServerCommands(ServerWindow server)
        {
            this.server = server;
            this.commands.Add("/server",cmdServer);
        }

        /*
        * Server command for specific server functions
        */
        public void cmdServer(String[] args)
        {
            int p;
            String txt;
            txt = "--- Server Help ---<br/>";
            txt += "/server start          | Start server on port " + server.port + "<br/>";
            txt += "/server start <port>   | Start server on specific port<br/>";
            txt += "/server stop           | Stop server<br/>";
            txt += "/server help           | Display this message<br/>";
            txt += "-------------------<br/>";
            for(int i = 0;i < args.Length;i++)
                args[i] = args[i].ToLower();
            if(args.Length == 1 && args[0] == "start")
                server.t.Start();
            else if(args.Length == 2 && args[0] == "start" && Int32.TryParse(args[1],out p))
            {
                server.port = p;
                server.t.Start();
            }
            else if(args.Length > 0 && args[0] == "stop")
            {
                server.listening = false;
                while(server.t.IsAlive);
                server.addText("Server stopped");
            }
            else if(args.Length > 0 && args[0].ToLower().Equals("help"))
            {
                server.addText(txt);
            }
            else
            {
                server.addText(txt);
            }
        }
    }
}
